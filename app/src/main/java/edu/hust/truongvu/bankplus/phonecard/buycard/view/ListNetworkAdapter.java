package edu.hust.truongvu.bankplus.phonecard.buycard.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.entity.MyNetwork;

/**
 * Created by truon on 3/28/2018.
 */

public class ListNetworkAdapter extends RecyclerView.Adapter<ListNetworkAdapter.NetworkHolder>{
    public interface NetworkListener{
        void onClick(MyNetwork myNetwork);
    }
    private NetworkListener mListener;
    private ArrayList<MyNetwork> listNetworks;
    private int mCheckedPosition = 0;

    public ListNetworkAdapter(ArrayList<MyNetwork> data, NetworkListener listener){
        this.listNetworks = data;
        this.mListener = listener;
    }

    @Override
    public NetworkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_network, parent, false);
        NetworkHolder holder = new NetworkHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(NetworkHolder holder, int position) {
        MyNetwork network = listNetworks.get(position);
        holder.setContent(network, position);
    }

    @Override
    public int getItemCount() {
        return listNetworks.size();
    }

    class NetworkHolder extends RecyclerView.ViewHolder{
        private TextView tvName;
        private RadioButton rdbButton;
        public NetworkHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_network);
            rdbButton = itemView.findViewById(R.id.rdb_network);
        }

        public void setContent(final MyNetwork myNetwork, final int position){
            tvName.setText(myNetwork.getName());

            if (position == mCheckedPosition){
                rdbButton.setChecked(true);
            }else {
                rdbButton.setChecked(false);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position == mCheckedPosition){
                        rdbButton.setChecked(false);
                        mCheckedPosition = 0;
                    }else {
                        mCheckedPosition = position;
                        notifyDataSetChanged();
                        mListener.onClick(myNetwork);
                    }
                }
            });

            rdbButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position == mCheckedPosition){
                        rdbButton.setChecked(false);
                        mCheckedPosition = 0;
                    }else {
                        mCheckedPosition = position;
                        notifyDataSetChanged();
                        mListener.onClick(myNetwork);
                    }
                }
            });
        }
    }
}
