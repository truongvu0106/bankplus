package edu.hust.truongvu.bankplus.phonecard.rechargephone.view;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.entity.PaymentHistory;

/**
 * Created by truon on 3/26/2018.
 */

public interface RechargePhoneView {
    void loadListPrice(ArrayList<Integer> list);
    void loadListFirstHistory(ArrayList<PaymentHistory> list);
    void loadMoreListHistory(ArrayList<PaymentHistory> list);
    void addSuccessful();
    void addFalse();
}
