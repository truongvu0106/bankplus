package edu.hust.truongvu.bankplus.phonecard.buycard.view;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.entity.MyNetwork;
import edu.hust.truongvu.bankplus.phonecard.buycard.presenter.BuyCardPresenterImp;
import edu.hust.truongvu.bankplus.phonecard.rechargephone.view.ListPriceAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class BuyCardFragment extends Fragment implements BuyCardView, View.OnClickListener{


    private View btnPay;
    private RecyclerView recyclerPrices, recyclerNetworks;
    private ListNetworkAdapter networkAdapter;
    private ListPriceAdapter priceAdapter;
    private BuyCardPresenterImp buyCardPresenterImp;
    public static BuyCardFragment getInstance(){
        BuyCardFragment buyCardFragment = new BuyCardFragment();
        return buyCardFragment;
    }

    public BuyCardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_buy_card, container, false);
        btnPay = view.findViewById(R.id.btn_pay);
        recyclerPrices = view.findViewById(R.id.list_card);
        recyclerNetworks = view.findViewById(R.id.list_network);
        buyCardPresenterImp = new BuyCardPresenterImp(getContext(), this);
        buyCardPresenterImp.initListNetwork();
        buyCardPresenterImp.initListPrice();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void loadListPrice(ArrayList<Integer> list) {
        priceAdapter = new ListPriceAdapter(getContext(), list, new ListPriceAdapter.PriceListener() {
            @Override
            public void onClick(int price) {

            }

            @Override
            public void onClickOther(String s) {

            }
        });
        recyclerPrices.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        recyclerPrices.setAdapter(priceAdapter);
    }

    @Override
    public void loadListNetwork(ArrayList<MyNetwork> list) {
        networkAdapter = new ListNetworkAdapter(list, new ListNetworkAdapter.NetworkListener() {
            @Override
            public void onClick(MyNetwork myNetwork) {

            }
        });
        recyclerNetworks.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        recyclerNetworks.setAdapter(networkAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btn_pay:
                break;
            default:
                break;
        }
    }
}
