package edu.hust.truongvu.bankplus.entity;

/**
 * Created by truon on 3/26/2018.
 */

public class PaymentHistory {
    private int id;
    private String datePayment;
    private String phoneNumber;
    private String money;
    private int type;

    public PaymentHistory(int id, String datePayment, String phoneNumber, String money, int type) {
        this.id = id;
        this.datePayment = datePayment;
        this.phoneNumber = phoneNumber;
        this.money = money;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDatePayment() {
        return datePayment;
    }

    public void setDatePayment(String datePayment) {
        this.datePayment = datePayment;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
