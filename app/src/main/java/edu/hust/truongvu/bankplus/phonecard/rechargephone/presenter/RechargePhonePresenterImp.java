package edu.hust.truongvu.bankplus.phonecard.rechargephone.presenter;

import android.content.Context;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.phonecard.rechargephone.model.RechargeModel;
import edu.hust.truongvu.bankplus.phonecard.rechargephone.view.RechargePhoneView;

/**
 * Created by truon on 3/26/2018.
 */

public class RechargePhonePresenterImp implements RechargePhonePresenter {
    private RechargePhoneView rechargePhoneView;
    private RechargeModel rechargeModel;
    public RechargePhonePresenterImp(Context context, RechargePhoneView rechargePhoneView){
        this.rechargePhoneView = rechargePhoneView;
        rechargeModel = new RechargeModel(context);
    }

    public RechargePhonePresenterImp(Context context){
        rechargeModel = new RechargeModel(context);
    }

    @Override
    public void initListPrice() {
        ArrayList<Integer> data = rechargeModel.getListPrices();
        rechargePhoneView.loadListPrice(data);
    }

    @Override
    public void addPaymentHistory() {

    }

    @Override
    public void initListHistory() {
        rechargeModel.openDatabase();
        rechargePhoneView.loadListFirstHistory(rechargeModel.getLimitPaymentHistory(3));
        rechargeModel.closeDatabase();
    }

    @Override
    public void loadMoreListHistory() {
        rechargeModel.openDatabase();
        rechargePhoneView.loadMoreListHistory(rechargeModel.getAllPaymentHistory());
        rechargeModel.closeDatabase();
    }
}
