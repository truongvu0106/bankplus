package edu.hust.truongvu.bankplus.phonecard.rechargephone.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.database.MyDb;
import edu.hust.truongvu.bankplus.entity.PaymentHistory;

import static edu.hust.truongvu.bankplus.database.MyDb.TABLE_NAME;

/**
 * Created by truon on 3/26/2018.
 */

public class RechargeModel {
    SQLiteDatabase database;
    private Context context;
    public RechargeModel(Context context){
        this.context = context;
    }

    public RechargeModel(){

    }

    public ArrayList<Integer> getListPrices(){
        ArrayList<Integer> data = new ArrayList<>();
        data.add(10000);
        data.add(20000);
        data.add(30000);
        data.add(50000);
        data.add(100000);
        data.add(200000);
        data.add(300000);
        data.add(500000);
        data.add(0);
        return data;
    }

    public void openDatabase(){
        MyDb myDb = new MyDb(context);
        database = myDb.getWritableDatabase();
    }

    public void closeDatabase(){
        database.close();
    }

    public boolean addPaymentHistory(PaymentHistory paymentHistory){
        ContentValues values = new ContentValues();
        values.put(MyDb.COLUMN_DATE, paymentHistory.getDatePayment());
        values.put(MyDb.COLUMN_PHONE, paymentHistory.getPhoneNumber());
        values.put(MyDb.COLUMN_MONEY, paymentHistory.getMoney());
        values.put(MyDb.COLUMN_TYPE, paymentHistory.getType());

        return database.insert(TABLE_NAME, null, values) > 0;
    }

    public void constructDatabase(){
        addPaymentHistory(new PaymentHistory(0, "20/3", "0983815459", "50000", 1));
        addPaymentHistory(new PaymentHistory(0, "20/3", "0983815459", "50000", 1));
        addPaymentHistory(new PaymentHistory(0, "20/3", "0983815459", "50000", 1));
        addPaymentHistory(new PaymentHistory(0, "20/3", "0983815459", "50000", 1));
        addPaymentHistory(new PaymentHistory(0, "20/3", "0983815459", "50000", 1));
        addPaymentHistory(new PaymentHistory(0, "20/3", "0983815459", "50000", 1));
        addPaymentHistory(new PaymentHistory(0, "20/3", "0983815459", "50000", 1));
    }

    public ArrayList<PaymentHistory> getAllPaymentHistory(){
        Cursor cursor = database.query(TABLE_NAME,
                new String[]{MyDb.COLUMN_ID, MyDb.COLUMN_DATE, MyDb.COLUMN_PHONE, MyDb.COLUMN_MONEY, MyDb.COLUMN_TYPE},
                null,
                null,
                null,
                null,
                MyDb.COLUMN_ID + " DESC");
        return convertCursorToPaymnet(cursor);
    }

    public ArrayList<PaymentHistory> getLimitPaymentHistory(int limit){
        Cursor cursor = database.query(TABLE_NAME,
                new String[]{MyDb.COLUMN_ID, MyDb.COLUMN_DATE, MyDb.COLUMN_PHONE, MyDb.COLUMN_MONEY, MyDb.COLUMN_TYPE},
                null,
                null,
                null,
                null,
                MyDb.COLUMN_ID + " DESC",
                limit + "");
        return convertCursorToPaymnet(cursor);
    }


    private ArrayList<PaymentHistory> convertCursorToPaymnet(Cursor cursor){
        if (cursor == null || cursor.getCount() <= 0){
            return new ArrayList<>();
        }
        ArrayList<PaymentHistory> paymentHistories = new ArrayList<>();
        cursor.moveToFirst();
        PaymentHistory paymentHistory;
        while (true) {
            int id = cursor.getInt(cursor.getColumnIndex(MyDb.COLUMN_ID));
            String date = cursor.getString(cursor.getColumnIndex(MyDb.COLUMN_DATE));
            String phone = cursor.getString(cursor.getColumnIndex(MyDb.COLUMN_PHONE));
            String money = cursor.getString(cursor.getColumnIndex(MyDb.COLUMN_MONEY));
            int type = cursor.getInt(cursor.getColumnIndex(MyDb.COLUMN_TYPE));

            paymentHistory = new PaymentHistory(id, date, phone, money, type);
            paymentHistories.add(paymentHistory);

            if (cursor.isLast()) {
                break;
            }
            cursor.moveToNext();
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return paymentHistories;
    }
}
