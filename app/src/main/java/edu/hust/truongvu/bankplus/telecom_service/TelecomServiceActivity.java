package edu.hust.truongvu.bankplus.telecom_service;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.customview.MyActionbar;
import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;


public class TelecomServiceActivity extends Activity {

    public static final String EXTRA_CODE = "code_type";
    View layoutToolbar;
    private TelecomService.Type mType =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telecom_service);

        RecyclerView list = findViewById(R.id.list);

        list.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        int type = getIntent().getIntExtra(EXTRA_CODE,-1);

        if(type<0){
            return;
        }

        List<TelecomService> services = new ArrayList<>();
        switch (mType = TelecomService.Type.values()[type]){
            case tv:{
                services = TelecomService.TVServices.getServices();
                break;
            }
            case internet:{
                services = TelecomService.InternetServices.getServices();
                break;
            }
            case phone:{
                services = TelecomService.PhoneServices.getServices();
                break;
            }
        }


        list.setAdapter(new TelecomServiceAdapter(services, new TelecomServiceAdapter.TelecomServiceListener() {
            @Override
            public void showDetail(TelecomService telecomService) {
                Intent intent = new Intent(TelecomServiceActivity.this,ServiceDetailActivity.class);
                intent.putExtra(ServiceDetailActivity.EXTRA_CODE,telecomService);
                startActivity(intent);
            }
        }));
        new MyActionbar(this);
    }

    @Override
    public String toString() {
        int name;
        switch (mType){
            case tv:{
                name = R.string.truyen_hinh;
                break;
            }
            case internet:{
                name = R.string.internet;
                break;
            }
            case phone:{
                name = R.string.dt_co_dinh;
                break;
            }
            default:
                return "";
        }
        return getString(name);
    }
}
