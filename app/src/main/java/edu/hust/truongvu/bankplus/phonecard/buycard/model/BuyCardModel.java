package edu.hust.truongvu.bankplus.phonecard.buycard.model;

import android.content.Context;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.entity.MyNetwork;

/**
 * Created by truon on 3/28/2018.
 */

public class BuyCardModel {
    private Context context;
    public BuyCardModel(Context context){
        this.context = context;
    }

    public ArrayList<MyNetwork> getListNetwork(){
        ArrayList<MyNetwork> arrayList = new ArrayList<>();
        arrayList.add(new MyNetwork(0, context.getString(R.string.viettel)));
        arrayList.add(new MyNetwork(1, context.getString(R.string.mobifone)));
        arrayList.add(new MyNetwork(2, context.getString(R.string.vietnamoblie)));
        arrayList.add(new MyNetwork(3, context.getString(R.string.vinaphone)));
        arrayList.add(new MyNetwork(4, context.getString(R.string.gmobile)));

        return arrayList;
    }

    public ArrayList<Integer> getListPrices(){
        ArrayList<Integer> data = new ArrayList<>();
        data.add(10000);
        data.add(20000);
        data.add(30000);
        data.add(50000);
        data.add(100000);
        data.add(200000);
        data.add(300000);
        data.add(500000);
        data.add(1000000);
        return data;
    }
}
