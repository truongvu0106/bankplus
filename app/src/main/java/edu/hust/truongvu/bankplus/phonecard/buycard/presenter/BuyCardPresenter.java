package edu.hust.truongvu.bankplus.phonecard.buycard.presenter;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.entity.MyNetwork;

/**
 * Created by truon on 3/26/2018.
 */

public interface BuyCardPresenter {
    void initListPrice();
    void initListNetwork();
}
