package edu.hust.truongvu.bankplus.entity;

/**
 * Created by truon on 3/28/2018.
 */

public class MyNetwork {
    private int id;
    private String name;

    public MyNetwork(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
