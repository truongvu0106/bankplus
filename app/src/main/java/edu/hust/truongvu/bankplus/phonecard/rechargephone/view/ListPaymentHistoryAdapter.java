package edu.hust.truongvu.bankplus.phonecard.rechargephone.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.entity.PaymentHistory;

/**
 * Created by truon on 3/28/2018.
 */

public class ListPaymentHistoryAdapter extends RecyclerView.Adapter<ListPaymentHistoryAdapter.PaymentHistoryViewHolder>{
    public interface PaymentHistoryListener{
        void onItemClick(PaymentHistory paymentHistory);
        void onDelete(PaymentHistory paymentHistory);
    }

    private PaymentHistoryListener mListener;
    private ArrayList<PaymentHistory> listPayment;

    public ListPaymentHistoryAdapter(ArrayList<PaymentHistory> data, PaymentHistoryListener listener){
        this.mListener = listener;
        this.listPayment = data;
    }

    @Override
    public PaymentHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        PaymentHistoryViewHolder holder = new PaymentHistoryViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PaymentHistoryViewHolder holder, int position) {
        PaymentHistory paymentHistory = listPayment.get(position);
        holder.setContent(paymentHistory);
    }

    @Override
    public int getItemCount() {
        return listPayment.size();
    }

    class PaymentHistoryViewHolder extends RecyclerView.ViewHolder{
        private TextView tvDate, tvPhone, tvMoney;
        private View btnDelete;
        public PaymentHistoryViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvPhone = itemView.findViewById(R.id.tv_phone_number);
            tvMoney = itemView.findViewById(R.id.tv_money);
            btnDelete = itemView.findViewById(R.id.btn_delete);
        }

        public void setContent(final PaymentHistory paymentHistory){
            tvDate.setText(paymentHistory.getDatePayment());
            tvPhone.setText(paymentHistory.getPhoneNumber());
            tvMoney.setText(paymentHistory.getMoney());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClick(paymentHistory);
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onDelete(paymentHistory);
                }
            });
        }
    }
}
