package edu.hust.truongvu.bankplus.telecom_service.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import edu.hust.truongvu.bankplus.R;


/**
 * Created by TrungBK on 28/03/2018.
 */

public class ViewBinderB {

    private View parent;
    private EditText editText;
    private Button button;
    private RadioButton radioButton1, radioButton2;
    private RadioGroup radioGroup;

    public ViewBinderB(Context context) {
        parent = View.inflate(context, R.layout.subview_telecom_service_b, null);
        editText = parent.findViewById(R.id.edt1);
        button = parent.findViewById(R.id.btn);
        radioButton1 = parent.findViewById(R.id.radio1);
        radioButton2 = parent.findViewById(R.id.radio2);
        radioGroup = parent.findViewById(R.id.radio_group);
    }

    public View getParent() {
        return parent;
    }

    public EditText getEditText() {
        return editText;
    }

    public Button getButton() {
        return button;
    }

    public RadioButton getRadioButton1() {
        return radioButton1;
    }

    public RadioButton getRadioButton2() {
        return radioButton2;
    }

    public RadioGroup getRadioGroup() {
        return radioGroup;
    }
}
