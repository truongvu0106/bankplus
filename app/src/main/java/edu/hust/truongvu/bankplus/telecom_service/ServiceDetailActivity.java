package edu.hust.truongvu.bankplus.telecom_service;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.customview.MyActionbar;
import edu.hust.truongvu.bankplus.telecom_service.control.TelecomServiceController;
import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;


public class ServiceDetailActivity extends Activity {

    public static final String EXTRA_CODE = "serviceCode";
    private TelecomService telecomService;
    private TelecomServiceController controller;

    private ImageView img;
    private TextView company;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        telecomService = getIntent().getParcelableExtra(EXTRA_CODE);
        new MyActionbar(this,TelecomService.InternetServices.IVIETTEL.isEqual(telecomService));

        img = findViewById(R.id.img);
        company = findViewById(R.id.tv_company);
        img.setImageResource(telecomService.getImgId());
        company.setText(telecomService.getCompany());

        controller = TelecomServiceController.getController(telecomService);
        if (controller == null) {
            return;
        }
        ViewGroup container = findViewById(R.id.container);
        container.addView(controller.getControlView(this));
    }

    @Override
    public String toString() {
        return telecomService.getName();
    }
}
