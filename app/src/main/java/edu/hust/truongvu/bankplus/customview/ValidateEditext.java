package edu.hust.truongvu.bankplus.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import edu.hust.truongvu.bankplus.R;

/**
 * Created by truon on 3/27/2018.
 */

@SuppressLint("AppCompatCustomView")
public class ValidateEditext extends EditText{
    boolean useValidate = false;
    String msg = "";
    public ValidateEditext(Context context) {
        super(context);
        construct(context, null);
    }

    public ValidateEditext(Context context, AttributeSet attrs) {
        super(context, attrs);
        construct(context, attrs);
    }

    public ValidateEditext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        construct(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ValidateEditext(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        construct(context, attrs);
    }

    private void construct(final Context context, AttributeSet attrs){
        if (attrs != null){
            TypedArray array = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.ValidateEditText, 0, 0);
            this.useValidate = array.getBoolean(R.styleable.ValidateEditText_useValidate, false);
            this.msg = array.getString(R.styleable.ValidateEditText_msg);
        }

        if (this.useValidate){
            setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    TextInputLayout textInputLayout = (TextInputLayout) view.getParent().getParent();
                    if (!b){
                        String text = getText().toString();
                        if (text.trim().matches("")){
                            textInputLayout.setErrorEnabled(true);
                            textInputLayout.setError(msg);
                        }
                    }else {
                        textInputLayout.setErrorEnabled(false);
                    }
                }
            });
        }

        setup();
    }

    private void setup(){
//        setInputType(InputType.TYPE_CLASS_TEXT | (isVisible?InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD:InputType.TYPE_TEXT_VARIATION_PASSWORD));
//        Drawable[] drawables = getCompoundDrawables();
//        myDrawable = (useHide && !isVisible)?drawableHide:drawableShow;
////        myDrawable.setAlpha(ALPHA);
//        setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], myDrawable,drawables[3]);
    }
}
