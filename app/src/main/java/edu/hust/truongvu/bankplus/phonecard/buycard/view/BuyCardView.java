package edu.hust.truongvu.bankplus.phonecard.buycard.view;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.entity.MyNetwork;

/**
 * Created by truon on 3/26/2018.
 */

public interface BuyCardView {
    void loadListPrice(ArrayList<Integer> list);
    void loadListNetwork(ArrayList<MyNetwork> list);
}
