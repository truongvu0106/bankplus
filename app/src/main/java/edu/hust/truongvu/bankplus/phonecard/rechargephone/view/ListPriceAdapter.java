package edu.hust.truongvu.bankplus.phonecard.rechargephone.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.R;

/**
 * Created by truon on 3/26/2018.
 */

public class ListPriceAdapter extends RecyclerView.Adapter<ListPriceAdapter.PriceViewHolder>{
    public interface PriceListener{
        void onClick(int price);
        void onClickOther(String s);
    }
    private PriceListener mListener;
    private ArrayList<Integer> mList;
    private Context context;
    private int mCheckedPosition = -1;

    public ListPriceAdapter(Context context, ArrayList<Integer> data, PriceListener listener){
        this.context = context;
        this.mList = data;
        this.mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 1;
        if (mList.get(position) == 0){
            viewType = 0;
        }
        return viewType;
    }

    @Override
    public PriceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        PriceViewHolder viewHolder;
        switch (viewType){
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_price_other, parent, false);
                break;
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_price, parent, false);
                break;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_price, parent, false);
                break;
        }
        viewHolder = new PriceViewHolder(view, viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PriceViewHolder holder, int position) {
        int price = mList.get(position);
        holder.setContent(price, position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class PriceViewHolder extends RecyclerView.ViewHolder{
        private int viewType;
        private TextView tvPrice;
        private View layoutCheck;
        public PriceViewHolder(View itemView, int viewType) {
            super(itemView);
            this.viewType = viewType;
            if (viewType == 1){
                tvPrice = itemView.findViewById(R.id.tv_number);
                layoutCheck = itemView.findViewById(R.id.layout_check);
            }
        }

        public void setContent(final int price, final int position){
            if (viewType == 1){
                tvPrice.setText(price + "");
                if (position == mCheckedPosition){
                    layoutCheck.setVisibility(View.VISIBLE);
                    tvPrice.setTextColor(context.getResources().getColor(R.color.white));
                }else {
                    layoutCheck.setVisibility(View.GONE);
                    tvPrice.setTextColor(context.getResources().getColor(R.color.my_gray));
                }
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (viewType == 1){
                        if (position == mCheckedPosition){
                            layoutCheck.setVisibility(View.GONE);
                            mCheckedPosition = -1;
                        }else {
                            mCheckedPosition = position;
                            notifyDataSetChanged();
                            mListener.onClick(price);
                        }
                    }else {
                        mListener.onClickOther(context.getString(R.string.tra_sau));
                    }
                }
            });
        }
    }
}
