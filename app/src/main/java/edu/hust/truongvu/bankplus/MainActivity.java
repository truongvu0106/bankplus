package edu.hust.truongvu.bankplus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import edu.hust.truongvu.bankplus.electronicwater.ElectronicActivity;
import edu.hust.truongvu.bankplus.electronicwater.WaterActivity;
import edu.hust.truongvu.bankplus.phonecard.PhoneCardActivity;
import edu.hust.truongvu.bankplus.telecom_service.TelecomServiceActivity;
import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private Button btnDienthoaiThecao;
    private Button btnTruyenhinh;
    private Button btnInternet;
    private Button btnDienthoaiban;
    private Button btnTiendien;
    private Button btnTiennuoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        btnDienthoaiThecao.setOnClickListener(this);
        btnTruyenhinh.setOnClickListener(this);
        btnInternet.setOnClickListener(this);
        btnDienthoaiban.setOnClickListener(this);
        btnTiendien.setOnClickListener(this);
        btnTiennuoc.setOnClickListener(this);

    }


    private void initView() {
        btnDienthoaiThecao = (Button) findViewById(R.id.btn_dienthoai_thecao);
        btnTruyenhinh = (Button) findViewById(R.id.btn_truyenhinh);
        btnInternet = (Button) findViewById(R.id.btn_internet);
        btnDienthoaiban = (Button) findViewById(R.id.btn_dienthoaiban);
        btnTiendien = (Button) findViewById(R.id.btn_tiendien);
        btnTiennuoc = (Button) findViewById(R.id.btn_tiennuoc);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_dienthoai_thecao:
                startActivity(new Intent(MainActivity.this, PhoneCardActivity.class));
                break;
            case R.id.btn_truyenhinh:
                Intent intent = new Intent(MainActivity.this, TelecomServiceActivity.class);
                intent.putExtra(TelecomServiceActivity.EXTRA_CODE, TelecomService.Type.tv.ordinal());
                startActivity(intent);
                break;
            case R.id.btn_internet:
                Intent intent1 = new Intent(MainActivity.this, TelecomServiceActivity.class);
                intent1.putExtra(TelecomServiceActivity.EXTRA_CODE, TelecomService.Type.internet.ordinal());
                startActivity(intent1);
                break;
            case R.id.btn_dienthoaiban:
                Intent intent2 = new Intent(MainActivity.this, TelecomServiceActivity.class);
                intent2.putExtra(TelecomServiceActivity.EXTRA_CODE, TelecomService.Type.phone.ordinal());
                startActivity(intent2);
                break;
            case R.id.btn_tiendien:
                startActivity(new Intent(MainActivity.this, ElectronicActivity.class));
                break;
            case R.id.btn_tiennuoc:
                startActivity(new Intent(MainActivity.this, WaterActivity.class));
                break;
            default:
                break;
        }
    }
}
