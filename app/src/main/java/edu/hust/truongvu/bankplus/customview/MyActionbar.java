package edu.hust.truongvu.bankplus.customview;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import edu.hust.truongvu.bankplus.R;

/**
 * Created by truon on 3/28/2018.
 */

public class MyActionbar {
    private Activity activity;
    private View btn_back;
    private TextView textView;
    private View chooseSample,line;


    public MyActionbar(final Activity activity) {
        this.activity = activity;
        btn_back = activity.findViewById(R.id.btn_back);
        textView = activity.findViewById(R.id.tvt_title);
        chooseSample = activity.findViewById(R.id.layout_choose_sample);
        if (btn_back != null && textView != null) {
            textView.setText(activity.toString());
            btn_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.onBackPressed();
                }
            });
            chooseSample.setVisibility(View.INVISIBLE);
        }
    }

    public void setChooseSample(boolean bChooseSample) {
        chooseSample.setVisibility(bChooseSample ? View.VISIBLE : View.INVISIBLE);
    }
    public MyActionbar(final Activity activity, boolean bChooseSample) {
        this.activity = activity;
        btn_back = activity.findViewById(R.id.btn_back);
        textView = activity.findViewById(R.id.tvt_title);
        line = activity.findViewById(R.id.line);
        chooseSample = activity.findViewById(R.id.layout_choose_sample);
        if (btn_back != null && textView != null) {
            textView.setText(activity.toString());
            btn_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.onBackPressed();
                }
            });
            chooseSample.setVisibility(bChooseSample ? View.VISIBLE : View.INVISIBLE);
        }
    }

    public MyActionbar(final Activity activity, boolean bChooseSample,boolean bLine) {
        this.activity = activity;
        btn_back = activity.findViewById(R.id.btn_back);
        textView = activity.findViewById(R.id.tvt_title);
        line = activity.findViewById(R.id.line);
        chooseSample = activity.findViewById(R.id.layout_choose_sample);
        if (btn_back != null && textView != null) {
            textView.setText(activity.toString());
            btn_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.onBackPressed();
                }
            });
            chooseSample.setVisibility(bChooseSample ? View.VISIBLE : View.INVISIBLE);
            line.setVisibility(bLine ? View.VISIBLE : View.INVISIBLE);
        }
    }


}
