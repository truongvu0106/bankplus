package edu.hust.truongvu.bankplus.telecom_service.control;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;
import edu.hust.truongvu.bankplus.telecom_service.view.ViewBinderB;


/**
 * Created by TrungBK on 26/03/2018.
 */

public class KPlusControler extends TelecomServiceController {

    private boolean state = true;

    @Override
    public TelecomService getService() {
        return TelecomService.TVServices.THKPLUS;
    }

    @Override
    public View getControlView(final Activity activity) {
        ViewBinderB binder = new ViewBinderB(activity);
        RadioButton radioButton1 = binder.getRadioButton1();
        RadioButton radioButton2 = binder.getRadioButton2();
        final Button button = binder.getButton();
        radioButton1.setText(R.string.gia_han_goi);
        radioButton2.setText(R.string.chuyen_goi_moi);

        binder.getButton().setText(R.string.thanh_toan);
        binder.getEditText().setHint(R.string.dau_giai_ma);

        radioButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setText(R.string.thanh_toan);
                state = true;
            }
        });
        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setText(R.string.kiem_tra);
                state = false;
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, getService().getName(), Toast.LENGTH_SHORT).show();
            }
        });
        radioButton1.setChecked(true);

        return binder.getParent();
    }
}
