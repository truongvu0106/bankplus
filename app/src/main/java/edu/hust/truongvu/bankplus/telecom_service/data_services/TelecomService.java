package edu.hust.truongvu.bankplus.telecom_service.data_services;

import android.os.Parcel;
import android.os.Parcelable;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.hust.truongvu.bankplus.R;


/**
 * Created by TrungBK on 26/03/2018.
 */

public class TelecomService implements Parcelable {

    public static class TVServices {
        public static final TelecomService THVIETTEL = new TelecomService(1, "Truyền hình Viettel", "Viettel", Type.tv, R.drawable.logo_nexttv,
                "Dịch vụ truyền hình số HD chất lượng cao, qua hạ tầng băng thông");
        public static final TelecomService THKPLUS = new TelecomService(2, "Truyền hình K+", "K+", Type.tv, R.drawable.logo_kplus,
                "Dịch vụ truyền hình số HD chất lượng cao, qua hạ tầng băng thông");
        public static final TelecomService VTVCAB = new TelecomService(3, "Truyền hình VTVCab", "Truyền hình VTVCab", Type.tv, R.drawable.vtvcab,
                "Dịch vụ truyền hình số HD chất lượng cao, qua hạ tầng băng thông");
        public static final TelecomService VTC = new TelecomService(4, "Truyền hình VTC", "VTC Media", Type.tv, R.drawable.logo_vtc,
                "Dịch vụ truyền hình số HD chất lượng cao, qua hạ tầng băng thông");
        public static final TelecomService MYTV_HCM = new TelecomService(5, "MyTV - VNPT HCM", "VNPT HCM", Type.tv, R.drawable.lg_vnpt_truyenhinh,
                "Dịch vụ truyền hình số HD chất lượng cao, qua hạ tầng băng thông");
        public static final TelecomService MYTV_HN = new TelecomService(6, "MyTV - VNPT HN", "VNPT Ha Noi", Type.tv, R.drawable.lg_vnpt_truyenhinh,
                "Dịch vụ truyền hình số HD chất lượng cao, qua hạ tầng băng thông");

        public static List<TelecomService> getServices() {
            TelecomService[] arr = {THVIETTEL, THKPLUS, VTVCAB, VTC, MYTV_HCM, MYTV_HN};
            List<TelecomService> services = Arrays.asList(arr);
            return services;
        }
    }

    public static class InternetServices {
        public static final TelecomService IVIETTEL = new TelecomService(1, "Internet Viettel", "Viettel", Type.internet, R.drawable.lg_viettel_internet,
                "Thanh toán cước internet ADSL, cáp quang ... của công ty");
        public static final TelecomService ISPT = new TelecomService(2, "Internet SPT", "Internet SPT", Type.internet, R.drawable.lg_sctv,
                "Thanh toán cước internet ADSL, cáp quang ... của công ty");
        public static final TelecomService VNPTHCM = new TelecomService(3, "VNPT HCM", "VNPT HCM", Type.internet, R.drawable.lg_vnpt,
                "Thanh toán cước internet ADSL, cáp quang ... của công ty");
        public static final TelecomService VNPTHN = new TelecomService(4, "VNPT Ho Noi", "VNPT Ha Noi", Type.internet, R.drawable.lg_vnpt,
                "Thanh toán cước internet ADSL, cáp quang ... của công ty");
        public static final TelecomService NSG = new TelecomService(5, "Internet Nam Sai Gon", "Nam Sai Gon", Type.internet, R.drawable.lg_namsaigon,
                "Dịch vụ truyền hình số HD chất lượng cao, qua hạ tầng băng thông");


        public static List<TelecomService> getServices() {
            TelecomService[] arr = {IVIETTEL, ISPT, VNPTHCM, VNPTHN, NSG};
            List<TelecomService> services = Arrays.asList(arr);
            return services;
        }
    }

    public static class PhoneServices {
        public static final TelecomService VTKHONGDAY = new TelecomService(1, "Viettel - Không dây (Home phone)", "Viettel",
                Type.phone, R.drawable.lg_viettel_homephone, "Thanh toán cước điện thoại cố định ... của công ty");
        public static final TelecomService VTCODAY = new TelecomService(2, "Viettel - Có dây", "Viettel",
                Type.phone, R.drawable.lg_viettel_codinh, "Thanh toán cước điện thoại cố định ... của công ty");
        public static final TelecomService NSG = new TelecomService(3, "Nam Sài Gòn", "Nam Sai Gon (SST)",
                Type.phone, R.drawable.lg_namsaigon, "Thanh toán cước điện thoại cố định ... của công ty");
        public static final TelecomService SPT = new TelecomService(4, "Điện thoại cố định SPT", "SPT",
                Type.phone, R.drawable.lg_sctv, "Thanh toán cước điện thoại cố định ... của công ty");
        public static final TelecomService VNPTHCM = new TelecomService(5, "VNPT HCM", "VNPT HCM",
                Type.phone, R.drawable.lg_vnpt, "Thanh toán cước điện thoại cố định ... của công ty");
        public static final TelecomService VNPTHN = new TelecomService(6, "VNPT Ha Noi", "VNPT Ha Noi",
                Type.phone, R.drawable.lg_vnpt, "Thanh toán cước điện thoại cố định ... của công ty");

        public static List<TelecomService> getServices() {
            TelecomService[] arr = {VTKHONGDAY, VTCODAY, NSG, SPT, VNPTHCM, VNPTHN};
            List<TelecomService> services = Arrays.asList(arr);
            return services;
        }
    }

    public enum Type {
        tv, internet, phone
    }

    private int id;
    private String name, company, description;
    private Type type;
    private int imgId;

    private TelecomService(int id, String name, String company, Type type, int imgId, String description) {
        this.id = id;
        this.name = name;
        this.company = company;
        this.type = type;
        this.imgId = imgId;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getCompany() {
        return company;
    }

    public Type getType() {
        return type;
    }

    public int getImgId() {
        return imgId;
    }

    public String getDescription() {
        return description;
    }

    public boolean isEqual(TelecomService service) {
        return (service.id == id) && (type == service.getType());
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.company);
        dest.writeString(this.description);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeInt(this.imgId);
    }

    protected TelecomService(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.company = in.readString();
        this.description = in.readString();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : Type.values()[tmpType];
        this.imgId = in.readInt();
    }

    public static final Creator<TelecomService> CREATOR = new Creator<TelecomService>() {
        @Override
        public TelecomService createFromParcel(Parcel source) {
            return new TelecomService(source);
        }

        @Override
        public TelecomService[] newArray(int size) {
            return new TelecomService[size];
        }
    };
}
