package edu.hust.truongvu.bankplus.phonecard.buycard.presenter;

import android.content.Context;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.entity.MyNetwork;
import edu.hust.truongvu.bankplus.phonecard.buycard.model.BuyCardModel;
import edu.hust.truongvu.bankplus.phonecard.buycard.view.BuyCardView;

/**
 * Created by truon on 3/26/2018.
 */

public class BuyCardPresenterImp implements BuyCardPresenter{
    private BuyCardView buyCardView;
    private BuyCardModel buyCardModel;
    public BuyCardPresenterImp(Context context, BuyCardView buyCardView){
        this.buyCardView = buyCardView;
        buyCardModel = new BuyCardModel(context);
    }

    @Override
    public void initListPrice() {
        buyCardView.loadListPrice(buyCardModel.getListPrices());
    }

    @Override
    public void initListNetwork() {
        buyCardView.loadListNetwork(buyCardModel.getListNetwork());
    }
}
