package edu.hust.truongvu.bankplus.phonecard.rechargephone.view;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.entity.PaymentHistory;
import edu.hust.truongvu.bankplus.phonecard.rechargephone.presenter.RechargePhonePresenterImp;


/**
 * A simple {@link Fragment} subclass.
 */
public class RechargePhoneFragment extends Fragment implements RechargePhoneView, View.OnClickListener{

    private EditText enterPhoneNumber, enterMoney;
    private View btnContact, btnPay;
    private RecyclerView recyclerPrice, recyclerHistory;
    private ListPriceAdapter adapter;
    private ListPaymentHistoryAdapter paymentHistoryAdapter;
    private View btnLoadMore;

    private RechargePhonePresenterImp rechargePhonePresenterImp;
    public static RechargePhoneFragment getInstance(){
        RechargePhoneFragment fragment = new RechargePhoneFragment();
        return fragment;
    }

    public RechargePhoneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recharge_phone, container, false);
        initView(view);

        rechargePhonePresenterImp = new RechargePhonePresenterImp(getContext(), this);
        rechargePhonePresenterImp.initListPrice();
        rechargePhonePresenterImp.initListHistory();

        btnLoadMore.setOnClickListener(this);
        btnContact.setOnClickListener(this);
        btnPay.setOnClickListener(this);

        return view;
    }


    private void initView(View view){
        enterPhoneNumber = view.findViewById(R.id.edt_phone);
        enterMoney = view.findViewById(R.id.edt_money);
        btnContact = view.findViewById(R.id.btn_contact);
        btnPay = view.findViewById(R.id.btn_pay);
        btnLoadMore = view.findViewById(R.id.btn_load_more);
        recyclerPrice = view.findViewById(R.id.list_price);
        recyclerHistory = view.findViewById(R.id.list_history);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btn_contact:
                break;
            case R.id.btn_pay:
                break;
            case R.id.btn_load_more:
                rechargePhonePresenterImp.loadMoreListHistory();
                break;
            default:
                break;
        }
    }

    @Override
    public void loadListPrice(ArrayList<Integer> list) {
        adapter = new ListPriceAdapter(getContext(), list, new ListPriceAdapter.PriceListener() {
            @Override
            public void onClick(int price) {
                enterMoney.setText(price + "");
            }

            @Override
            public void onClickOther(String s) {
                enterMoney.setText(s);
            }
        });
        recyclerPrice.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        recyclerPrice.setAdapter(adapter);
    }

    @Override
    public void loadListFirstHistory(ArrayList<PaymentHistory> list) {
        paymentHistoryAdapter = new ListPaymentHistoryAdapter(list, new ListPaymentHistoryAdapter.PaymentHistoryListener() {
            @Override
            public void onItemClick(PaymentHistory paymentHistory) {

            }

            @Override
            public void onDelete(PaymentHistory paymentHistory) {

            }
        });
        recyclerHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerHistory.setAdapter(paymentHistoryAdapter);
    }

    @Override
    public void loadMoreListHistory(ArrayList<PaymentHistory> list) {
        btnLoadMore.setVisibility(View.GONE);
        paymentHistoryAdapter = new ListPaymentHistoryAdapter(list, new ListPaymentHistoryAdapter.PaymentHistoryListener() {
            @Override
            public void onItemClick(PaymentHistory paymentHistory) {

            }

            @Override
            public void onDelete(PaymentHistory paymentHistory) {

            }
        });
        recyclerHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerHistory.setAdapter(paymentHistoryAdapter);
    }

    @Override
    public void addSuccessful() {

    }

    @Override
    public void addFalse() {

    }
}
