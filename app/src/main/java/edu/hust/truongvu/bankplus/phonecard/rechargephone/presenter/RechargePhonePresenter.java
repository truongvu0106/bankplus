package edu.hust.truongvu.bankplus.phonecard.rechargephone.presenter;

/**
 * Created by truon on 3/26/2018.
 */

public interface RechargePhonePresenter {
    void initListPrice();
    void addPaymentHistory();
    void initListHistory();
    void loadMoreListHistory();
}
