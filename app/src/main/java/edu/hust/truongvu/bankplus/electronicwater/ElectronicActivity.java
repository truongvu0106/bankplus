package edu.hust.truongvu.bankplus.electronicwater;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.customview.MyActionbar;

public class ElectronicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electronic);
        new MyActionbar(this);
    }

    @Override
    public String toString() {
        return getString(R.string.tien_dien);

    }
}
