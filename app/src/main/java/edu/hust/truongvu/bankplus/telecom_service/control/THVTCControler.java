package edu.hust.truongvu.bankplus.telecom_service.control;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;
import edu.hust.truongvu.bankplus.telecom_service.view.ViewBinderC;

/**
 * Created by TrungBK on 26/03/2018.
 */

public class THVTCControler extends TelecomServiceController {
    @Override
    public TelecomService getService() {
        return TelecomService.TVServices.VTC;
    }

    @Override
    public View getControlView(final Activity activity) {
        final ViewBinderC binder = new ViewBinderC(activity);
        binder.getEdt1().setHint(R.string.nhap_ma_hd);
        binder.getTitle().setText(R.string.so_tien);
        binder.getBtn().setText(R.string.thanh_toan);

        List<String> categories = new ArrayList<String>();
        categories.add("400.000 (SD)");
        categories.add("720.000 (SD)");
        categories.add("720.000 (HD)");
        categories.add("1.200.000 (HD)");
        final String[] mStrings = {"Bốn trăm nghìn đồng", "Bảy trăm hai mươi nghìn đồng",
                "Bảy trăm hai mươi nghìn đồng", "Một triệu hai trăm nghìn đồng"};

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binder.getSpinner().setAdapter(dataAdapter);
        binder.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binder.getNote().setText(activity.getString(R.string.bang_chu) + ": " + mStrings[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binder.getSpinner().setSelection(0);
        binder.getNote().setText(activity.getString(R.string.bang_chu) + ": " + mStrings[0]);

        binder.getBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, getService().getName(), Toast.LENGTH_SHORT).show();
            }
        });

        return binder.getParent();
    }
}
