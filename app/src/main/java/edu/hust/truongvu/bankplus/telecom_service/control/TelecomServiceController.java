package edu.hust.truongvu.bankplus.telecom_service.control;

import android.app.Activity;
import android.view.View;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;


/**
 * Created by TrungBK on 26/03/2018.
 */

public abstract class TelecomServiceController {

    public static TelecomServiceController getController(TelecomService service) {
        TelecomServiceController[] arr = {
                new THViettelControler(), new KPlusControler(), new VTVCabControler(), new THVTCControler(), new MyTVHCMControler(), new MyTVHNControler(),
                new InternetNSGControler(), new InternetSPTControler(), new InternetViettelControler(), new InternetVNPTHCMControler(), new InternetVNPTHNControler(),
                new PhoneViettelKhongDayControler(),new PhoneNSGControler(),new PhoneSPTControler(), new PhoneVNPTHCMControler(), new PhoneVNPTHNControler(),
                new PhoneVTCoDayControler()
        };
        List<TelecomServiceController> controllers = Arrays.asList(arr);
        for (TelecomServiceController controller : controllers) {
            if (controller.getService().isEqual(service)) {
                return controller;
            }
        }
        return null;
    }

    public abstract TelecomService getService();

    public abstract View getControlView(Activity activity);
}
