package edu.hust.truongvu.bankplus.telecom_service.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import edu.hust.truongvu.bankplus.R;


/**
 * Created by TrungBK on 28/03/2018.
 */

public class ViewBinderC {

    private View parent;
    private EditText edt1;
    private TextView title;
    private Spinner spinner;
    private TextView note;
    private Button btn;


    public ViewBinderC(Context context) {
        parent = View.inflate(context, R.layout.subview_telecom_service_c, null);
        initView();
    }


    private void initView() {
        edt1 = (EditText) parent.findViewById(R.id.edt1);
        title = (TextView) parent.findViewById(R.id.title);
        spinner = (Spinner) parent.findViewById(R.id.spinner);
        note = (TextView) parent.findViewById(R.id.note);
        btn = (Button) parent.findViewById(R.id.btn);
    }

    public View getParent() {
        return parent;
    }

    public EditText getEdt1() {
        return edt1;
    }

    public TextView getTitle() {
        return title;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public TextView getNote() {
        return note;
    }

    public Button getBtn() {
        return btn;
    }
}
