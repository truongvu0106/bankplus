package edu.hust.truongvu.bankplus.telecom_service.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import edu.hust.truongvu.bankplus.R;


/**
 * Created by TrungBK on 28/03/2018.
 */

public class ViewBinderD {

    public interface ViewDListener {
        void onClick(String sdt, int money);

        void traCuu(String sdt, int money);
    }

    private View parent;
    private EditText editText2;
    private Button button;
    private ViewDListener listener;

    public ViewBinderD(Context context, final ViewDListener listener) {
        this.listener = listener;
        parent = View.inflate(context, R.layout.subview_telecom_service_d, null);
        editText2 = parent.findViewById(R.id.edt2);
        button = parent.findViewById(R.id.btn);

        if (listener != null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(editText2.getText().toString(), Integer.parseInt("0"));
                }
            });
        }

        setNumberEdt();

    }

    private void setNumberEdt() {
        editText2.addTextChangedListener(new NumberTextWatcher(editText2));
    }

    public View getParent() {
        return parent;
    }

    public Button getButton() {
        return button;
    }
}
