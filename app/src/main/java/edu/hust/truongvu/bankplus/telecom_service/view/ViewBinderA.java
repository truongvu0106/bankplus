package edu.hust.truongvu.bankplus.telecom_service.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import edu.hust.truongvu.bankplus.R;


/**
 * Created by TrungBK on 28/03/2018.
 */

public class ViewBinderA {

    public interface ViewAListener {
        int getHint();

        int getTextButton();

        void onClick(String content);
    }

    private View parent;
    private EditText editText;
    private Button button;
    private ViewAListener listener;

    public ViewBinderA(Context context, final ViewAListener listener) {
        this.listener = listener;
        parent = View.inflate(context, R.layout.subview_telecom_service_a, null);
        editText = parent.findViewById(R.id.edt1);
        button = parent.findViewById(R.id.btn);

        if (listener != null) {
            button.setText(listener.getTextButton());
            editText.setHint(listener.getHint());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(editText.getText().toString());
                }
            });
        }
    }

    public View getParent() {
        return parent;
    }

    public EditText getEditText() {
        return editText;
    }

    public Button getButton() {
        return button;
    }
}
