package edu.hust.truongvu.bankplus.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by truon on 3/28/2018.
 */

public class MyDb extends SQLiteOpenHelper {
    private static final String DB_NAME = "bankplus";
    private static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "payment_history";
    public static final String COLUMN_ID = "id_payment";
    public static final String COLUMN_DATE = "date_payment";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_MONEY = "money";
    public static final String COLUMN_TYPE = "type_payment";

    public MyDb(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String cr_table = "create table " + TABLE_NAME + " ( " + COLUMN_ID + " integer primary key autoincrement,"
                + COLUMN_DATE + " text, "
                + COLUMN_PHONE + " text, "
                + COLUMN_MONEY + " text, "
                + COLUMN_TYPE + " integer " + ");";
        sqLiteDatabase.execSQL(cr_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }


}
