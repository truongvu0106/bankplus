package edu.hust.truongvu.bankplus.telecom_service.control;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;
import edu.hust.truongvu.bankplus.telecom_service.view.ViewBinderA;


/**
 * Created by TrungBK on 26/03/2018.
 */

public class MyTVHCMControler extends TelecomServiceController {
    @Override
    public TelecomService getService() {
        return TelecomService.TVServices.MYTV_HCM;
    }

    @Override
    public View getControlView(final Activity activity) {
        ViewBinderA binder = new ViewBinderA(activity, new ViewBinderA.ViewAListener() {
            @Override
            public int getHint() {
                return R.string.nhap_ma_hd;
            }

            @Override
            public int getTextButton() {
                return R.string.tiep_tuc;
            }

            @Override
            public void onClick(String content) {
                Toast.makeText(activity, getService().getName(), Toast.LENGTH_SHORT).show();
            }
        });
        return binder.getParent();
    }
}
