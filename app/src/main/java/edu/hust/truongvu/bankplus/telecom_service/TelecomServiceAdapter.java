package edu.hust.truongvu.bankplus.telecom_service;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;


/**
 * Created by TrungBK on 26/03/2018.
 */

public class TelecomServiceAdapter extends RecyclerView.Adapter<TelecomSeviceHolder> {

    private List<TelecomService> services;
    private TelecomServiceListener listener;

    public TelecomServiceAdapter(@NonNull List<TelecomService> services, TelecomServiceListener listener) {
        this.services = services;
        this.listener = listener;
    }

    public interface TelecomServiceListener {
        void showDetail(TelecomService telecomService);
    }

    @Override
    public TelecomSeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TelecomSeviceHolder(
                View.inflate(parent.getContext(), R.layout.item_telecom_service,null),
                listener
        );
    }

    @Override
    public void onBindViewHolder(TelecomSeviceHolder holder, int position) {
        holder.bind(services.get(position));
    }

    @Override
    public int getItemCount() {
        return services.size();
    }
}

class TelecomSeviceHolder extends RecyclerView.ViewHolder {

    private ImageView img;
    private TextView name, description;

    private TelecomServiceAdapter.TelecomServiceListener listener;
    private TelecomService telecomService;

    public TelecomSeviceHolder(View itemView, final TelecomServiceAdapter.TelecomServiceListener listener) {
        super(itemView);
        this.listener = listener;
        img = itemView.findViewById(R.id.img);
        name = itemView.findViewById(R.id.tv_name);
        description = itemView.findViewById(R.id.tv_description);
        if (listener != null) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.showDetail(telecomService);
                }
            });
        }
    }

    public void bind(TelecomService telecomService) {
        this.telecomService = telecomService;
        name.setText(telecomService.getName());
        img.setImageResource(telecomService.getImgId());
        description.setText(telecomService.getDescription());
    }

}
