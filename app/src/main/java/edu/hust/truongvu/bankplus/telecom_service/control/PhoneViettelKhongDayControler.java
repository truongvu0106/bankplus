package edu.hust.truongvu.bankplus.telecom_service.control;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import edu.hust.truongvu.bankplus.telecom_service.data_services.TelecomService;
import edu.hust.truongvu.bankplus.telecom_service.view.ViewBinderD;


/**
 * Created by TrungBK on 26/03/2018.
 */

public class PhoneViettelKhongDayControler extends TelecomServiceController {
    @Override
    public TelecomService getService() {
        return TelecomService.PhoneServices.VTKHONGDAY;
    }

    @Override
    public View getControlView(final Activity activity) {
        ViewBinderD binder = new ViewBinderD(activity, new ViewBinderD.ViewDListener() {
            @Override
            public void onClick(String sdt, int money) {

            }

            @Override
            public void traCuu(String sdt, int money) {

            }
        });
        return binder.getParent();
    }
}
