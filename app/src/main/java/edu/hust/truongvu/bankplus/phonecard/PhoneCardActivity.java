package edu.hust.truongvu.bankplus.phonecard;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import edu.hust.truongvu.bankplus.R;
import edu.hust.truongvu.bankplus.customview.MyActionbar;
import edu.hust.truongvu.bankplus.phonecard.rechargephone.model.RechargeModel;
import edu.hust.truongvu.bankplus.phonecard.rechargephone.view.RechargePhoneFragment;
import edu.hust.truongvu.bankplus.phonecard.buycard.view.BuyCardFragment;

public class PhoneCardActivity extends AppCompatActivity{

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View layoutChooseSample;
    private RechargeModel rechargeModel;
    MyActionbar myActionbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_card);
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.main_viewpager);
        layoutChooseSample = findViewById(R.id.layout_choose_sample);
        setupWithViewPager();
        tabLayout.setupWithViewPager(viewPager);
        myActionbar = new MyActionbar(this, true, false);
        rechargeModel = new RechargeModel(this);
        rechargeModel.openDatabase();
        rechargeModel.constructDatabase();
        rechargeModel.closeDatabase();
    }

    private void setupWithViewPager(){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(RechargePhoneFragment.getInstance(), getString(R.string.nap_tien_dien_thoai));
        adapter.addFragment(BuyCardFragment.getInstance(), getString(R.string.mua_ma_the_cao));
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                myActionbar.setChooseSample(position == 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public String toString() {
        return getString(R.string.dienthoai_thecao);
    }
}
